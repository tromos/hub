// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package pipeline // import "gitlab.com/tromos/hub/processor/pipeline/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/processor/module/aes"
	"gitlab.com/tromos/hub/processor/module/echo"
	"gitlab.com/tromos/hub/processor/module/mirror"
	"gitlab.com/tromos/hub/processor/module/reedsolomon"
	"gitlab.com/tromos/hub/processor/module/sha256"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Pipeline{}
}

type Pipeline struct{}

func (d *Pipeline) Reusable() bool {
	return false
}

func (d *Pipeline) Upstream(b processor.Builder) {
	b.Add("feed", &echo.Uplink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewUplink())
	b.Connect("feed", "Out", "checksum", "In")

	b.Add("encrypt", aes.NewUplink(0, "lolabunnysixteen"))
	b.Connect("checksum", "Out", "encrypt", "In")

	b.Add("mirror", mirror.NewUplink())
	b.Connect("encrypt", "Out", "mirror", "In")

	b.Add("erasure", reedsolomon.NewUplink(1, 1, 102400))
	b.Connect("mirror", "Out", "erasure", "In")

	b.Add("sink0", &echo.Uplink{})
	b.Connect("erasure", "Out", "sink0", "In")

	b.Add("sink1", &echo.Uplink{})
	b.Connect("erasure", "Out", "sink1", "In")

	b.MapOutPort("s0", "sink0", "Out")
	b.MapOutPort("s1", "sink1", "Out")
}

func (d *Pipeline) Downstream(b processor.Builder) {
	b.Add("feed", &echo.Downlink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewDownlink())
	b.Connect("feed", "Out", "checksum", "In")

	b.Add("encrypt", aes.NewDownlink(0, "lolabunnysixteen"))
	b.Connect("checksum", "Out", "encrypt", "In")

	b.Add("mirror", mirror.NewDownlink())
	b.Connect("encrypt", "Out", "mirror", "In")

	b.Add("erasure", reedsolomon.NewDownlink(1, 1, 102400))
	b.Connect("mirror", "Out", "erasure", "In")

	b.Add("sink0", &echo.Downlink{})
	b.Connect("erasure", "Out", "sink0", "In")

	b.Add("sink1", &echo.Downlink{})
	b.Connect("erasure", "Out", "sink1", "In")

	b.MapOutPort("s0", "sink0", "Out")
	b.MapOutPort("s1", "sink1", "Out")

}
