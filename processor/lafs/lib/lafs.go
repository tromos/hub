// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package lafs // import "gitlab.com/tromos/hub/processor/lafs/lib"

import (
	"code.cloudfoundry.org/bytefmt"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/processor/module/aes"
	"gitlab.com/tromos/hub/processor/module/echo"
	"gitlab.com/tromos/hub/processor/module/reedsolomon"
	"gitlab.com/tromos/hub/processor/module/sha256"
	"gitlab.com/tromos/hub/selector"
)

func New(config *viper.Viper) processor.ProcessGraph {

	g := &graph{}

	// Validate Integritycheck
	g.integritycheck = config.GetBool("integritycheck")

	// Validate Encryption
	g.encrypt = config.GetBool("encrypt")

	// Validate Passphrase
	g.passphrase = config.GetString("passphrase")
	if g.encrypt && g.passphrase == "" {
		panic("Empty passphrase is not allowed")
	}

	// Validate datablocks
	if config.GetInt("datablocks") == 0 {
		panic("0 datablocks are not allowed")
	} else {
		g.datablocks = config.GetInt("datablocks")
	}

	// Validate parity blocks
	if config.GetInt("parityblocks") == 0 {
		panic("0 parityblocks are not allowed")
	} else {
		g.parityblocks = config.GetInt("parityblocks")
	}

	// Validate bloskzei
	bs, err := bytefmt.ToBytes(config.GetString("blocksize"))
	if err != nil {
		panic(err)
	}
	g.blocksize = int(bs)

	logrus.Info("Parameters:", g.String())
	return g
}

type graph struct {
	integritycheck bool
	encrypt        bool
	passphrase     string
	datablocks     int
	parityblocks   int
	blocksize      int
}

func (graph *graph) String() string {
	return fmt.Sprintf("Integrity check %t, Encryption %t Datablocks %d ParityBlocks %d Blocksize %d", graph.integritycheck, graph.encrypt, graph.datablocks, graph.parityblocks, graph.blocksize)
}

func (*graph) Reusable() bool {
	return false
}

func (graph *graph) Upstream(b processor.Builder) {

	// Handle checksum
	b.Add("echo0", &echo.Uplink{})
	if graph.integritycheck {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "echo0", "In")
	} else {
		b.MapInPort("In", "echo0", "In")
	}

	// Handle encryption
	b.Add("erasure", reedsolomon.NewUplink(
		graph.datablocks,
		graph.parityblocks,
		graph.blocksize,
	))
	if graph.encrypt {
		b.Add("encrypt", aes.NewUplink(0, graph.passphrase))
		b.Connect("echo0", "Out", "encrypt", "In")
		b.Connect("encrypt", "Out", "erasure", "In")
	} else {
		b.Connect("echo0", "Out", "erasure", "In")
	}

	// Handle erasure-coding
	var dataechos int
	for i := 0; i < graph.datablocks; i++ {
		echoID := fmt.Sprintf("data%v", i)
		b.Add(echoID, &echo.Uplink{})
		b.Connect("erasure", "Out", echoID, "In")

		b.MapOutPort(echoID, echoID, "Out", selector.DataStream)
		dataechos = i
	}

	for i := 0; i < graph.parityblocks; i++ {
		echoID := fmt.Sprintf("par%v", i+dataechos+1)
		b.Add(echoID, &echo.Uplink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.ParityStream)
	}
}

func (graph *graph) Downstream(b processor.Builder) {

	// Handle checksum
	b.Add("echo0", &echo.Downlink{})
	if graph.integritycheck {
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "echo0", "In")
	} else {
		b.MapInPort("In", "echo0", "In")
	}

	// Handle encryption
	b.Add("erasure", reedsolomon.NewDownlink(
		graph.datablocks,
		graph.parityblocks,
		graph.blocksize,
	))
	if graph.encrypt {
		b.Add("encrypt", aes.NewDownlink(0, graph.passphrase))
		b.Connect("echo0", "Out", "encrypt", "In")
		b.Connect("encrypt", "Out", "erasure", "In")
	} else {
		b.Connect("echo0", "Out", "erasure", "In")
	}

	// Handle erasure-coding
	var dataechos int
	for i := 0; i < graph.datablocks; i++ {
		echoID := fmt.Sprintf("data%v", i)
		b.Add(echoID, &echo.Downlink{})
		b.Connect("erasure", "Out", echoID, "In")

		b.MapOutPort(echoID, echoID, "Out", selector.DataStream)
		dataechos = i
	}

	for i := 0; i < graph.parityblocks; i++ {
		echoID := fmt.Sprintf("par%v", i+dataechos+1)
		b.Add(echoID, &echo.Downlink{})

		b.Connect("erasure", "Out", echoID, "In")
		b.MapOutPort(echoID, echoID, "Out", selector.ParityStream)
	}
}
