// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package direct // import "gitlab.com/tromos/hub/processor/direct/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/processor/module/echo"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Direct{}
}

type Direct struct{}

func (d *Direct) Reusable() bool {
	return true
}

func (d *Direct) Upstream(b processor.Builder) {
	b.Add("echo", &echo.Uplink{})

	b.MapInPort("In", "echo", "In")
	b.MapOutPort("s0", "echo", "Out")
}

func (d *Direct) Downstream(b processor.Builder) {
	b.Add("echo", &echo.Downlink{})

	b.MapInPort("In", "echo", "In")
	b.MapOutPort("s0", "echo", "Out")
}
