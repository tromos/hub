// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/hub/processor"

import (
	"gitlab.com/tromos/hub/selector"
)

// Ensure that DefaultProcessor struct implements the Processor interface
var _ Processor = (*DefaultProcessor)(nil)

type DefaultProcessor struct {
	closed bool
}

func (d *DefaultProcessor) String() string {
	if d.closed {
		panic(ErrClosed)
	}
	return "DefaultProcessor"
}

func (d *DefaultProcessor) Location() string {
	if d.closed {
		panic(ErrClosed)
	}

	return "localhost"
}

func (d *DefaultProcessor) Capabilities() []selector.Capability {
	if d.closed {
		panic(ErrClosed)
	}

	return []selector.Capability{selector.Default}
}

func (d *DefaultProcessor) NewChannel(_ ChannelConfig) (Channel, error) {
	return &DefaultChannel{}, nil
}

func (d *DefaultProcessor) Close() error {
	if d.closed {
		panic(ErrClosed)
	}
	d.closed = true

	return nil
}

/*
func (DefaultProcessor) Add(name string, module interface{}) bool {
	return false
}

func (DefaultProcessor) MapInPort(port, app, appPort string) {
	panic("Methods should not be called on DefaultProcessor")
}


func (DefaultProcessor) Connect(senderName, senderPort, receiverName, receiverPort string) bool {
	return false
}

func (DefaultProcessor) 	MapOutPort(port, app, appPort string, capabilities ...selector.Capability) {
	panic("Methods should not be called on DefaultProcessor")
}
*/

// Ensure that DefaultChannel struct implements the Channel interface
var _ Channel = (*DefaultChannel)(nil)

type DefaultChannel struct {
	closed bool
	sinks  map[string]string
}

func (ch *DefaultChannel) Config() ChannelConfig {
	if ch.closed {
		panic(ErrClosed)
	}
	return ChannelConfig{}
}

func (ch *DefaultChannel) Capabilities(port string) []selector.Capability {
	if ch.closed {
		panic(ErrClosed)
	}
	return nil
}

func (ch *DefaultChannel) NewTransfer(stream Stream) error {
	if ch.closed {
		panic(ErrClosed)
	}
	return nil
}

func (ch *DefaultChannel) Sinks() map[string]string {
	if ch.closed {
		panic(ErrClosed)
	}
	return nil
}

func (ch *DefaultChannel) Close() error {
	if ch.closed {
		panic(ErrClosed)
	}
	ch.closed = true
	return nil

}
