// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package dedup // import "gitlab.com/tromos/hub/processor/module/dedup"

import (
	"github.com/klauspost/dedup"
	"gitlab.com/tromos/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func (up *Uplink) OnIn(in processor.Stream) {
	pr, pw := processor.Pipe()

	up.Out <- processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	// Create a new writer, with each block being 1000 bytes,
	// And allow it to use 10000 bytes of memory
	w, err := dedup.NewStreamWriter(pw, dedup.ModeFixed, 1000, 10000)
	if err != nil {
		panic(err)
	}
	defer w.Close()

	_, err = io.Copy(w, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func (down *Downlink) OnIn(in processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	// Create a new stream reader:
	r, err := dedup.NewStreamReader(pr)
	if err != nil {
		panic(err)
	}
	defer r.Close()

	_, err = io.Copy(in.Data, r)
	if err != nil && err != io.EOF {
		panic(err)
	}
}
