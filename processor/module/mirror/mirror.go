// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package mirror // import "gitlab.com/tromos/hub/processor/module/mirror"

import (
	"github.com/djherbis/stream"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	//	"gitlab.com/tromos/hub/pkg/log"
	"fmt"
	"io"
)

type Uplink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out []chan<- processor.Stream

	memfs stream.FileSystem
}

func NewUplink() *Uplink {
	return &Uplink{
		logger: logrus.WithField("module", "mirror"),
		memfs:  stream.NewMemFS(),
	}
}

func (up *Uplink) OnIn(in processor.Stream) {
	up.logger.Info("Received stream:", in.Port)

	w, err := stream.NewStream("", up.memfs)
	if err != nil {
		panic(err)
	}

	go func() {
		if _, err := io.Copy(w, in.Data); err != nil {
			panic(err)
		}
		w.Close()
	}()

	for i := 0; i < len(up.Out); i++ {
		r, err := w.NextReader()
		if err != nil {
			panic(err)
		}
		defer r.Close()

		pr, pw := processor.Pipe()
		up.Out[i] <- processor.Stream{
			Data: pr,
			Port: fmt.Sprintf("mirror:%d", i),
			Meta: in.Meta,
		}

		if _, err := io.Copy(pw, r); err != nil {
			panic(err)
		}

		if err := pw.Close(); err != nil {
			panic(err)
		}
	}
}

type Downlink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out []chan<- processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{
		logger: logrus.WithField("module", "mirror"),
	}
}

func (down *Downlink) OnIn(in processor.Stream) {
	// Always read from the first replicate
	// FIXME: add selection algorithm (probably another module)
	down.Out[0] <- in

	for i := 1; i < len(down.Out); i++ {
		down.Out[i] <- processor.Stream{}
	}
}
