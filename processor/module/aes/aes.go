// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package aes // import "gitlab.com/tromos/hub/processor/module/aes"

import (
	"crypto/aes"
	"crypto/cipher"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	"io"
)

type Uplink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream

	mode int
	key  string
}

func NewUplink(mode int, key string) *Uplink {
	return &Uplink{
		logger: logrus.WithField("module", "aes"),
		mode:   mode,
		key:    key,
	}
}

func (up *Uplink) OnIn(in processor.Stream) {
	up.logger.Info("Received stream:", in.Port)

	pr, pw := processor.Pipe()
	defer pw.Close()
	up.Out <- processor.Stream{
		Data: pr,
		Port: "aes",
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(up.key))
	if err != nil {
		panic(err)
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])
	encrypter := &cipher.StreamWriter{S: stream, W: pw}
	defer encrypter.Close()

	_, err = io.Copy(encrypter, in.Data)
	if err != nil {
		panic(err)
	}
}

type Downlink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream

	mode int
	key  string
}

func NewDownlink(mode int, key string) *Downlink {

	return &Downlink{
		logger: logrus.WithField("module", "aes"),
		mode:   mode,
		key:    key,
	}
}

func (down *Downlink) OnIn(in processor.Stream) {
	down.logger.Info("Received stream:", in.Port)

	pr, pw := processor.Pipe()
	down.Out <- processor.Stream{
		Data: pw,
		Port: "aes",
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(down.key))
	if err != nil {
		in.Data.CloseWithError(err)
		return
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])

	decrypter := &cipher.StreamReader{S: stream, R: pr}

	if _, err := io.Copy(in.Data, decrypter); err != nil {
		in.Data.CloseWithError(err)
		return
	}

	in.Data.Close()
}
