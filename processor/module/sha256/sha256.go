// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package sha256 // import "gitlab.com/tromos/hub/processor/module/sha256"

import (
	"crypto/sha256"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	"io"
)

type Uplink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func NewUplink() *Uplink {
	return &Uplink{
		logger: logrus.WithField("module", "sha256"),
	}
}

func (up *Uplink) OnIn(in processor.Stream) {
	up.logger.Info("Received stream:", in.Port)

	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- processor.Stream{
		Data: pr,
		Port: "sha256",
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(in.Data, pw)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
	}

	in.Meta.SetState("checksum", fmt.Sprintf("%x", h.Sum(nil)))
}

type Downlink struct {
	logger *logrus.Entry

	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{
		logger: logrus.WithField("module", "sha256"),
	}
}

func (down *Downlink) OnIn(in processor.Stream) {
	down.logger.Info("Received stream:", in.Port)

	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- processor.Stream{
		Data: pw,
		Port: "sha256",
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(pr, in.Data)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
		return
	}

	if fmt.Sprintf("%x", h.Sum(nil)) != in.Meta.GetStateKey("checksum") {
		if err := in.Data.CloseWithError(processor.ErrCorrupted); err != nil {
			panic(err)
		}
		return
	}
}
