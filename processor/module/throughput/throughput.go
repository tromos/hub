// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package throughput // import "gitlab.com/tromos/hub/processor/module/throughput"

import (
	"fmt"
	"github.com/aybabtme/iocontrol"
	"gitlab.com/tromos/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In     <-chan processor.Stream
	Out    chan<- processor.Stream
	writer *iocontrol.MeasuredWriter
}

func (up *Uplink) Finish() {
	if up.writer == nil {
		return
	}
	fmt.Println("Uptream bytes per sec: ", up.writer.BytesPerSec())
}

func (up *Uplink) OnIn(in processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	up.writer = iocontrol.NewMeasuredWriter(pw)

	_, err := io.Copy(up.writer, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In     <-chan processor.Stream
	Out    chan<- processor.Stream
	reader *iocontrol.MeasuredReader
}

func (down *Downlink) Finish() {
	if down.reader == nil {
		return
	}
	fmt.Println("Downstream bytes per sec: ", down.reader.BytesPerSec())
}

func (down *Downlink) OnIn(in processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	down.reader = iocontrol.NewMeasuredReader(pr)

	_, err := io.Copy(in.Data, down.reader)
	if err != nil {
		panic(err)
	}
}
