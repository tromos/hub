// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package gzip // import "gitlab.com/tromos/hub/processor/module/gzip"

import (
	"compress/gzip"
	"gitlab.com/tromos/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func (up *Uplink) OnIn(in processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	compressor, err := gzip.NewWriterLevel(pw, gzip.BestSpeed)
	if err != nil {
		panic(err)
	}
	defer compressor.Close()

	_, err = io.Copy(compressor, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In  <-chan processor.Stream
	Out chan<- processor.Stream
}

func (down *Downlink) OnIn(in processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	decompressor, err := gzip.NewReader(pr)
	if err != nil {
		panic(err)
	}
	defer decompressor.Close()

	_, err = io.Copy(in.Data, decompressor)
	if err != nil {
		panic(err)
	}
}
