// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package raid1 // import "gitlab.com/tromos/hub/processor/raid1/lib"

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/processor/module/aes"
	"gitlab.com/tromos/hub/processor/module/echo"
	"gitlab.com/tromos/hub/processor/module/mirror"
	"gitlab.com/tromos/hub/processor/module/sha256"
)

// Options
// Replicas: Number of replicas
// Encrypt: whether to encrypt the replicas
// IntegrityCheck: whether to implement integrirty checking
func New(config *viper.Viper) processor.ProcessGraph {

	g := &graph{}

	// Validate Integritycheck
	g.integritycheck = config.GetBool("integritycheck")

	// Validate Encryption
	g.encrypt = config.GetBool("encrypt")

	// Validate Passphrase
	g.passphrase = config.GetString("passphrase")
	if g.encrypt && g.passphrase == "" {
		panic("Empty passphrase is not allowed")
	}

	// Validate replicas
	if config.GetInt("replicas") == 0 {
		panic("0 replicas are not allowed")
	} else {
		g.replicas = config.GetInt("replicas")
	}

	return g
}

type graph struct {
	replicas       int
	encrypt        bool
	integritycheck bool
	passphrase     string
}

func (*graph) Reusable() bool {
	return true
}

// Bottom-up description, with intermediate placeholders
func (graph *graph) Upstream(b processor.Builder) {

	// Handle checksum
	b.Add("echo0", &echo.Uplink{})
	if graph.integritycheck {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "echo0", "In")
	} else {
		b.MapInPort("In", "echo0", "In")
	}

	// Handle encryption
	b.Add("mirror", mirror.NewUplink())
	if graph.encrypt {
		b.Add("encrypt", aes.NewUplink(0, graph.passphrase))
		b.Connect("echo0", "Out", "encrypt", "In")
		b.Connect("encrypt", "Out", "mirror", "In")
	} else {
		b.Connect("echo0", "Out", "mirror", "In")
	}

	// Handle mirroring
	for i := 0; i < graph.replicas; i++ {
		echoID := fmt.Sprintf("sink%v", i)
		b.Add(echoID, &echo.Uplink{})
		b.Connect("mirror", "Out", echoID, "In")

		b.MapOutPort(echoID, echoID, "Out")
	}
}

// Top-down description, without intermediate placeholders. Starting from the most
// specific to the more generic
func (graph *graph) Downstream(b processor.Builder) {

	// Handle checksum
	b.Add("echo0", &echo.Downlink{})
	if graph.integritycheck {
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "echo0", "In")
	} else {
		b.MapInPort("In", "echo0", "In")
	}

	// Handle encryption
	b.Add("mirror", mirror.NewDownlink())
	if graph.encrypt {
		b.Add("encrypt", aes.NewDownlink(0, graph.passphrase))
		b.Connect("echo0", "Out", "encrypt", "In")
		b.Connect("encrypt", "Out", "mirror", "In")
	} else {
		b.Connect("echo0", "Out", "mirror", "In")
	}

	// Handle mirroring
	for i := 0; i < graph.replicas; i++ {
		echoID := fmt.Sprintf("sink%v", i)
		b.Add(echoID, &echo.Downlink{})
		b.Connect("mirror", "Out", echoID, "In")

		b.MapOutPort(echoID, echoID, "Out")
	}
}
