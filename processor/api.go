// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/hub/processor"

import (
	"context"
	"github.com/spf13/viper"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"io"
	"io/ioutil"
	"sync"
)

// Module is a struct that must be embeded to all the components that want to
// be integerated into Processor. The Processor expects the module to triggger
// signals for all its connected ports. For modules like mirror whether some
// of the ports are not really used, the module must send to its outputs a
// closed pipe reader so for the Processor to gracefully terminate the line
type Module struct {
	flow.Component
	ID string
}

// Builder is used to compile a Meta-graph into a DAG
// A Meta-graph is a graph that defines all the possible datapaths that will be used
type Builder interface {
	// Add adds a new process with a given name to the DAG
	Add(name string, module interface{}) bool

	// MapInPort adds an inport to the DAG and maps it to a contained module's port.
	MapInPort(port, app, appPort string)

	// Connect connects a sender to a receiver module
	Connect(senderName, senderPort, receiverName, receiverPort string) bool

	// MapOutPort adds an outport to the DAG  and maps it to a contained module's port.
	// Capability contain linking information with the next element (Processor or Device)
	MapOutPort(port, app, appPort string, capabilities ...selector.Capability)
}

type Plugin func(conf *viper.Viper) ProcessGraph

// ProcessGraph are the drivers of the Processors.
// Should be used only by the Graph plugin Developers
type ProcessGraph interface {
	// Reusable returns whether a datapath can be used from a pool of precompile instances
	// or it has to be compiled at runtime
	Reusable() bool
	Upstream(b Builder)
	Downstream(b Builder)
}

type ChannelConfig struct {
	Context      context.Context
	Writable     bool
	Name         string
	Sinks        map[string]string `validate:"required"`
	WalkID       []string
	Capabilities []selector.Capability
	Pushout      func(stream Stream) `validate:"required"`
}

func (cc *ChannelConfig) FillMissingFields() {
	if cc.Context == nil {
		cc.Context = context.Background()
	}

	if cc.Name == "" {
		cc.Name = uuid.Once()
	}

	if cc.Sinks == nil {
		cc.Sinks = make(map[string]string)
	}

	if cc.WalkID == nil {
		cc.WalkID = []string{"127.0.0.1"}
	}

	if cc.Pushout == nil {
		if cc.Writable {
			cc.Pushout = DefaultPushoutUpstream
		} else {
			cc.Pushout = DefaultPushoutDownstream
		}
	}
}

// DefaultPushoutUpstream mocks the device functionality for upstream
func DefaultPushoutUpstream(stream Stream) {
	pr := stream.Data.(Receiver).PipeReader
	_, err := io.Copy(ioutil.Discard, pr)
	if err != nil {
		if err := stream.Data.CloseWithError(err); err != nil {
			panic(err)
		}
	} else {
		if err := stream.Data.Close(); err != nil {
			panic(err)
		}
	}
}

// DefaultPushoutDownstream will return dummy data equal to stream.Totalsize
func DefaultPushoutDownstream(stream Stream) {
	pw := stream.Data.(Sender).PipeWriter
	_, err := pw.Write(make([]byte, stream.Meta.TotalSize))
	if err != nil {
		if err := stream.Data.CloseWithError(err); err != nil {
			panic(err)
		}
	} else {
		if err := stream.Data.Close(); err != nil {
			panic(err)
		}
	}
}

// Processor is a continuously running instnace that spawn channels. It is the Device
// counterpart for the compute plane
type Processor interface {
	// String returns a descriptive name for the Processor
	String() string

	// Location returns the node where the Processor is running
	Location() string

	// Capabilities returns the capabilities of the Processor
	Capabilities() []selector.Capability

	// NewChannel returns a new channel associated to the Processor
	NewChannel(config ChannelConfig) (Channel, error)

	// Close gracefully shutdowns the processor
	Close() error
}

// Channel is a channel request for Processor
type Channel interface {
	// Config returns the channel configuration
	Config() ChannelConfig

	// Capabilities return tags of the output ports. Tags can be used to resolve compabibility
	// with the components to be linked (either Processors or Devices). Similarly, the can be
	// used as arguments in the device selection process. They are useful only for the upstream
	Capabilities(port string) []selector.Capability

	// NewTransfer registers the intentions synchronously and perform the data copy
	// asynchronously like in a future/promise model.
	//
	// The error indicates a mishappen during preparation phase.
	// Completion and errors are signalled back through pipe (see PipeError)
	NewTransfer(stream Stream) error

	// ReadyPort returns a channel for listenting asynchronous events that occur on the ports
	// The Port of event corresponds to the index as returned of Outports()
	//Readyports() <-chan Stream

	// Sinks returns all the devices affected by the reserved datapath
	Sinks() map[string]string

	// Close blocks until all the pending streams are flushed and terminates
	// the channel reservation. Second call to close should cause a panic.
	// If there are any errors occurred in the streams, Close() returns ErrStream.
	Close() error
}

// Stream is an end-to-end asynchronous transfer descriptor
type Stream struct {
	Data ReadWriteCloser `validate:"required" json:"-"` // Convey data
	// The port of the parent which started this stream
	Port string `json:"Port" validate:"required"`

	Meta *StreamMetadata `validate:"required"` // Convey metadata
}

// TransferState is an information packet for maintaining the state of the stream
type StreamMetadata struct {
	locker sync.Mutex
	// Identifier unique within the Channel
	ID int `json:"-"`
	// TotalSize returns the overall size of the Stream
	TotalSize int
	// State store changes on the processsing plane
	State map[string]string `validate:"required_with=TotalSize" json:"State,omitempty"`
	// Items store changes on the storage plane
	Items map[string]device.Item `validate:"required_with=TotalSize" json:"Items,omitempty" `
}

// SetState add a new entry to the state map. If the map is empty is allocates a new one
func (s *StreamMetadata) SetState(key string, value string) {
	s.locker.Lock()
	if s.State == nil {
		s.State = make(map[string]string)
	}
	s.State[key] = value
	s.locker.Unlock()
}

// SetItem add a new entry to the item map.  If the map is empty is allocates a new one
func (s *StreamMetadata) SetItem(key string, value device.Item) {
	s.locker.Lock()
	if s.Items == nil {
		s.Items = make(map[string]device.Item)
	}
	s.Items[key] = value
	s.locker.Unlock()
}

// GetItem returns the item identified by key
func (s *StreamMetadata) GetItem(key string) (device.Item, bool) {
	s.locker.Lock()
	item, ok := s.Items[key]
	s.locker.Unlock()
	return item, ok
}

// GetState returns the state for the given keys. If no keys are defined, the
// whole state field is return
func (s *StreamMetadata) GetState(keys ...string) map[string]string {
	s.locker.Lock()
	defer s.locker.Unlock()

	if len(keys) == 0 {
		return s.State
	}

	state := make(map[string]string)
	for _, key := range keys {
		state[key] = s.State[key]
	}
	return state
}

func (s *StreamMetadata) GetStateKey(key string) string {
	s.locker.Lock()
	defer s.locker.Unlock()

	return s.State[key]
}
