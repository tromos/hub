// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package filesystem // import "gitlab.com/tromos/hub/device/filesystem/lib"

import (
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/pkg/path"
	"gitlab.com/tromos/hub/selector"
	"os"
)

func New(conf *viper.Viper) device.Device {
	var fs afero.Fs
	switch conf.GetString("family") {
	case "memory":
		fs = afero.NewMemMapFs()
	case "os":
		fs = afero.NewOsFs()
	default:
		panic("Unrecognized filesystem family (Os, Mem)")
	}

	// Container and Remove Container work with ID  (e.g full path)
	// while CreateContainer with key (e.g filename).
	pathDir := conf.GetString("path")
	if pathDir == "" {
		panic("path not defined")
	}
	rootID := pathDir + "/root"

	if conf.GetBool("cleanstart") {
		if err := fs.RemoveAll(rootID); err != nil {
			panic(err)
		}
	}

	if err := path.CreateIfNotExists(rootID, true); err != nil {
		panic(err)
	}

	return &Filesystem{
		Fs:   fs,
		root: rootID,
	}
}

type Filesystem struct {
	conf *viper.Viper
	afero.Fs
	root string
}

func (fs *Filesystem) SetBackend(_ device.Device) {}

func (fs *Filesystem) String() string {
	return "filesystem:" + fs.root
}

func (fs *Filesystem) Capabilities() []selector.Capability {
	if fs.conf.GetString("family") == "memory" {
		return []selector.Capability{selector.InMemory}
	}
	return nil
}

func (fs *Filesystem) Location() string {
	return ""
}

func (fs *Filesystem) Close() error {
	return nil
}

func (fs *Filesystem) NewWriteChannel(name string) (device.WriteChannel, error) {
	return &wchannel{
		Fs:     fs.Fs,
		prefix: fs.root + "/" + name,
	}, nil
}

func (fs *Filesystem) NewReadChannel(_ string) (device.ReadChannel, error) {
	return &rchannel{Fs: fs.Fs}, nil
}

func (fs *Filesystem) Scan() ([]string, []device.Item, error) {
	files := make([]string, 0)
	items := make([]device.Item, 0)
	err := afero.Walk(fs.Fs, fs.root, func(path string, info os.FileInfo, err error) error {
		// Ignore directories, pipes, sockets, ...
		if info.Mode().IsRegular() {
			files = append(files, info.Name())

			items = append(items, device.Item{
				Size: uint64(info.Size()),
				ID:   path,
			})
		}
		return nil
	})
	return files, items, err
}
