// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package filesystem // import "gitlab.com/tromos/hub/device/filesystem/lib"

import (
	"github.com/spf13/afero"
	"gitlab.com/tromos/hub/device"
	"io"
)

type rchannel struct {
	afero.Fs
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	file, err := ch.Fs.Open(in.Item.ID)
	if err != nil {
		return err
	}
	/* -- Question: is it better to fetch the chunks into memory
	* or to directly access the medium ?
		if _, err := file.Seek(item.Offset, 0); err != nil {
			return err
		}

		if _, err := io.CopyN(dst, file, item.Size); err != nil {
			return err
		}
	*/
	go func() {
		defer dst.Close()
		defer file.Close()

		reader := io.NewSectionReader(file, int64(in.Item.Offset), int64(in.Item.Size))
		if _, err := io.Copy(dst, reader); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		if err := file.Close(); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
	}()
	return nil
}
