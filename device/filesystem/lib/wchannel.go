// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package filesystem // import "gitlab.com/tromos/hub/device/filesystem/lib"

import (
	"fmt"
	"github.com/spf13/afero"
	"gitlab.com/tromos/hub/device"
	"io"
	"sync"
)

type transfer struct {
	in  *device.Stream
	out afero.File
}

type wchannel struct {
	afero.Fs

	prefix string

	transferLocker sync.Mutex
	transfers      []*transfer

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		return device.ErrClosed
	}
	ch.closed = true

	for _, transfer := range ch.transfers {
		<-transfer.in.Complete
	}
	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, in *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pid := len(ch.transfers)

	// Create a data holder
	filepath := fmt.Sprintf("%v.%v", ch.prefix, pid)
	file, err := ch.Fs.Create(filepath)
	if err != nil {
		return err
	}

	transfer := &transfer{
		in:  in,
		out: file,
	}
	ch.transfers = append(ch.transfers, transfer)

	// Perform the data transfer asynchronously
	go func() {
		wb, err := io.Copy(transfer.out, src)
		if err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
		if err := transfer.out.Sync(); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
		if err := transfer.out.Close(); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		transfer.in.Item.Size = uint64(wb)
		transfer.in.Item.ID = filepath

		close(transfer.in.Complete)
	}()
	return nil
}
