// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package protocol // import "gitlab.com/tromos/hub/device/proxy/rpc/protocol"

import (
	"gitlab.com/tromos/hub/device"
	"log"
)

// errors is a interface, thus its scope is only insitu.
// to be able to propagate error we use masked error codes instead
type ErrCode int

const (
	CodeOK      ErrCode = 0
	CodeProxy           = 1
	CodeBackend         = 2
	CodeNoImpl          = 3
)

func MaskError(err error) ErrCode {
	switch err {
	case nil:
		return CodeOK

	case device.ErrBackend:
		return CodeBackend

	case device.ErrNoImpl:
		return CodeNoImpl

	default:
		log.Printf("Uncaptured error %v", err)
		return CodeProxy
	}
}

func UnmaskError(code ErrCode) error {
	switch code {
	case CodeOK:
		return nil

	case CodeBackend:
		return device.ErrBackend

	case CodeNoImpl:
		return device.ErrNoImpl

	default:
		return device.ErrProxy
	}
}
