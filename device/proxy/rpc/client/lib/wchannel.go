// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package proxy // import "gitlab.com/tromos/hub/device/proxy/rpc/client/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/device/proxy/rpc/protocol"
	"io"
	"sync"
)

// transfer describes the glue between an in transfer and an out connection.
// complete is needed because a transfer may be finished, but the metadata of the transfer
// will be available only after the channel closing. Thereby, the two completion signals
// must be distinct (otherwise it would signal the completion without metadata)
type transfer struct {
	in       *device.Stream
	out      *yamux.Stream
	complete chan error
}

type wchannel struct {
	remote  *WebServiceOperations
	chanID  string
	session *yamux.Session

	transferLocker sync.Mutex
	transfers      []*transfer

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	// Drain  flying streams
	for _, transfer := range ch.transfers {
		if err := <-transfer.complete; err != nil {
			return device.ErrStream
		}
	}

	// Prohibite future streams
	metadata, errcode := ch.remote.WchClose(ch.chanID)
	if err := protocol.UnmaskError(errcode); err != nil {
		return err
	}

	// Prohibite future streams
	<-ch.session.CloseChan()

	// Stabilize metadata
	for i, transfer := range ch.transfers {
		transfer.in.Item = metadata[i]
		close(transfer.in.Complete)
	}

	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, in *device.Stream) error {
	ch.transferLocker.Lock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	// Handshare a connection
	errcode := ch.remote.WchNewtransfer(ch.chanID)
	if err := protocol.UnmaskError(errcode); err != nil {
		return err
	}

	// Initialize the data transfer
	conn, err := ch.session.OpenStream()
	if err != nil {
		return err
	}

	transfer := &transfer{
		in:       in,
		out:      conn,
		complete: make(chan error),
	}
	ch.transfers = append(ch.transfers, transfer)

	// Perform the data transfer asynchronously
	go func() {
		ch.transferLocker.Unlock()
		if _, err := io.Copy(conn, src); err != nil {
			transfer.complete <- err
			return
		}

		if err := transfer.out.Close(); err != nil {
			transfer.complete <- err
			return
		}

		close(transfer.complete)
	}()
	return nil
}
