// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package proxy // import "gitlab.com/tromos/hub/device/proxy/rpc/client/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/device/proxy/rpc/protocol"
	"io"
)

type rchannel struct {
	remote *WebServiceOperations
	chanID string

	session *yamux.Session
}

func (ch *rchannel) Close() (err error) {
	errcode := ch.remote.RchClose(ch.chanID)
	return protocol.UnmaskError(errcode)
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, stream *device.Stream) error {
	errcode := ch.remote.RchWriteto(ch.chanID, stream.Item)
	if err := protocol.UnmaskError(errcode); err != nil {
		return err
	}

	// Open a new stream
	go func() {
		defer dst.Close()
		conn, err := ch.session.Open()
		if err != nil {
			panic(err)
		}
		defer conn.Close()

		if _, err = io.Copy(dst, conn); err != nil {
			panic(err)
		}
	}()
	return nil
}
