// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package proxy // import "gitlab.com/tromos/hub/device/proxy/rpc/server/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/hub/device"
	"io"
	"sync"
)

type transfer struct {
	in       *yamux.Stream
	out      *device.Stream
	complete chan error
}

type wchannel struct {
	remote device.WriteChannel

	transferLocker sync.Mutex
	transfers      []*transfer

	session *yamux.Session

	closed bool
}

func (ch *wchannel) Close() ([]device.Item, error) {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	// Drain the flying streams
	for _, transfer := range ch.transfers {
		if err := <-transfer.complete; err != nil {
			return nil, device.ErrStream
		}
	}

	// Prohibite future streams
	if err := ch.session.Close(); err != nil {
		return nil, err
	}

	// Get the backend metadata
	if err := ch.remote.Close(); err != nil {
		return nil, err
	}

	var metadata []device.Item
	for _, transfer := range ch.transfers {
		metadata = append(metadata, transfer.out.Item)
	}
	return metadata, nil
}

func (ch *wchannel) NewStream() error {

	ch.transferLocker.Lock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	// No error checking is needed. This is a client's task
	out := &device.Stream{Complete: make(chan struct{})}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, out); err != nil {
		return err
	}

	transfer := &transfer{
		out:      out,
		complete: make(chan error),
	}

	go func() {
		in, err := ch.session.AcceptStream()
		if err != nil {
			transfer.complete <- err
			return
		}
		transfer.in = in

		ch.transfers = append(ch.transfers, transfer)
		ch.transferLocker.Unlock()

		if _, err := io.Copy(pw, in); err != nil {
			transfer.complete <- err
			return
		}
		if err := pw.Close(); err != nil {
			transfer.complete <- err
			return
		}
		close(transfer.complete)
	}()
	return nil
}
