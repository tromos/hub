// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package proxy // import "gitlab.com/tromos/hub/device/proxy/rpc/server/lib"

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel

	transferLocker sync.Mutex
	session        *yamux.Session

	closed bool
}

func (ch *rchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(stream *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pw, stream); err != nil {
		return err
	}

	go func() {
		in, err := ch.session.AcceptStream()
		if err != nil {
			panic(err)
		}

		if _, err := io.Copy(in, pr); err != nil {
			panic(err)
		}

		if err := in.Close(); err != nil {
			panic(err)
		}

	}()

	return nil
}
