// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package proxy // import "gitlab.com/tromos/hub/device/proxy/rpc/server/lib"

import (
	"context"
	"github.com/hashicorp/yamux"
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"github.com/orcaman/concurrent-map"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/device/proxy/rpc/protocol"
	"net"
	"net/http"
)

func New(conf *viper.Viper) device.Device {

	host := conf.GetString("host")
	if host == "" {
		panic("host is missing")
	}

	port := conf.GetString("port")
	if port == "" {
		panic("port is missing")
	}

	return &Server{
		host:      host,
		port:      port,
		wchannels: cmap.New(),
		rchannels: cmap.New(),
	}
}

type Server struct {
	device.Device
	host       string
	port       string
	webservice *http.Server
	wchannels  cmap.ConcurrentMap // Contains device.WriteChannel
	rchannels  cmap.ConcurrentMap // Contains device.ReadChannel
}

func (srv *Server) SetBackend(backend device.Device) {
	srv.Device = backend

	// Lazy initialization - if there is no backend, there is no point in
	// running a server
	ops := rpc.NewWebSocketService()
	ops.AddFunction("ProxyString", backend.String)
	ops.AddFunction("ProxyCapabilities", backend.Capabilities)
	ops.AddFunction("ProxyScan", srv.ProxyScan)
	ops.AddFunction("ProxyNewWriteChannel", srv.ProxyNewWriteChannel)
	ops.AddFunction("WCH_NewTransfer", srv.WchNewtransfer)
	ops.AddFunction("WCH_Close", srv.WchClose)
	ops.AddFunction("ProxyNewReadChannel", srv.ProxyNewReadChannel)
	ops.AddFunction("RCH_WriteTo", srv.RchWriteto)
	ops.AddFunction("RCH_Close", srv.RchClose)
	ops.AddFunction("Close", srv.Close)

	webservice := &http.Server{
		Addr:    srv.host + ":" + srv.port,
		Handler: ops,
	}
	srv.webservice = webservice

	go func() {
		if err := webservice.ListenAndServe(); err != nil {
			panic(err)
		}
	}()
}

func (srv *Server) Close() error {
	if err := srv.webservice.Shutdown(context.TODO()); err != nil {
		return err
	}
	return srv.Device.Close()
}

func (srv *Server) ProxyScan() ([]string, []device.Item, protocol.ErrCode) {
	ids, items, err := srv.Device.Scan()
	if err != nil {
		return nil, nil, protocol.MaskError(err)
	}
	return ids, items, protocol.CodeOK
}

func (srv *Server) ProxyNewWriteChannel(name string) (string, protocol.ErrCode) {
	remote, err := srv.Device.NewWriteChannel(name)
	if err != nil {
		return "", protocol.MaskError(err)
	}

	// Dedicate a listener to serve the channel
	// port 0 let's the kernel decide the available port
	listener, err := net.Listen("tcp", srv.host+":0")
	if err != nil {
		return "", protocol.MaskError(err)
	}
	chanID := listener.Addr().String()

	ch := &wchannel{remote: remote}
	ch.transferLocker.Lock()

	srv.wchannels.Set(chanID, ch)

	go func() {
		// Accept a TCP connection
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		// Setup server side of yamux
		session, err := yamux.Server(conn, yamux.DefaultConfig())
		if err != nil {
			panic(err)
		}
		ch.session = session
		ch.transferLocker.Unlock()
	}()

	return chanID, protocol.CodeOK
}

func (srv *Server) WchNewtransfer(chanID string) protocol.ErrCode {
	chi, ok := srv.wchannels.Get(chanID)
	if !ok {
		panic("Invalid Channel ID ")
	}
	ch := chi.(*wchannel)

	return protocol.MaskError(ch.NewStream())
}

func (srv *Server) WchClose(chanID string) ([]device.Item, protocol.ErrCode) {
	chi, ok := srv.wchannels.Get(chanID)
	if !ok {
		panic("Invalid Channel ID ")
	}
	ch := chi.(*wchannel)
	defer srv.wchannels.Remove(chanID)

	// retrieve the stream metadata
	metadata, err := ch.Close()
	return metadata, protocol.MaskError(err)
}

func (srv *Server) ProxyNewReadChannel(name string) (string, protocol.ErrCode) {
	remote, err := srv.Device.NewReadChannel(name)
	if err != nil {
		return "", protocol.MaskError(err)
	}

	// Dedicate a listener to serve the channel
	// port 0 let's the kernel decide the available port
	listener, err := net.Listen("tcp", srv.host+":0")
	if err != nil {
		return "", protocol.MaskError(err)
	}
	chanID := listener.Addr().String()

	ch := &rchannel{remote: remote}
	ch.transferLocker.Lock()

	srv.rchannels.Set(chanID, ch)

	go func() {
		// Accept a TCP connection
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		// Setup server side of yamux
		session, err := yamux.Server(conn, yamux.DefaultConfig())
		if err != nil {
			panic(err)
		}
		ch.session = session
		ch.transferLocker.Unlock()
	}()

	return chanID, protocol.CodeOK
}

func (srv *Server) RchWriteto(chanID string, item device.Item) protocol.ErrCode {
	chi, ok := srv.rchannels.Get(chanID)
	if !ok {
		panic("Invalid Channel ID ")
	}
	ch := chi.(*rchannel)

	return protocol.MaskError(ch.NewTransfer(&device.Stream{Item: item}))
}

func (srv *Server) RchClose(chanID string) protocol.ErrCode {
	chi, ok := srv.rchannels.Get(chanID)
	if !ok {
		panic("Invalid Channel ID ")
	}
	ch := chi.(*rchannel)

	return protocol.MaskError(ch.Close())
}
