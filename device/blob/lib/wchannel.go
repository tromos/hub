// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package blob // import "gitlab.com/tromos/hub/device/blob/lib"

import (
	"bytes"
	"gitlab.com/tromos/hub/device"
	"io"
	"sync"
)

// contain information as to the number of chunks where the data of a
// stream are placed. The index of (blobs, outChunkSize, outChunkOffset) contain
// the chunk information
type transfer struct {
	in             *device.Stream
	outBlobs       []int   // Physical (blo) location of chunk
	outChunkSize   []int64 // Size of chunk (within the blob)
	outChunkOffset []int64 // Offset of chunk within the blob
}

type wchannel struct {
	remote device.WriteChannel

	transferLocker sync.Mutex
	transfers      []*transfer

	bufLocker sync.Mutex
	blobs     []*device.Stream
	buffer    []byte
	boffset   int64

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	if err := ch.flush(); err != nil {
		return err
	}

	// Blocks until all blob metadata are stable
	if err := ch.remote.Close(); err != nil {
		return err
	}

	// The data of a stream may be stream across several physical blobs.
	// In such case, the physical items should be placed in chain
	for _, transfer := range ch.transfers {
		for i := 0; i < len(transfer.outBlobs); i++ {
			if transfer.in.Item.IsEmpty() {
				// Use the physical blob location as the stream's location
				transfer.in.Item.ID = ch.blobs[transfer.outBlobs[i]].Item.ID
				transfer.in.Item.Size = uint64(transfer.outChunkSize[i])
				transfer.in.Item.Offset = uint64(transfer.outChunkOffset[i])
			} else {
				transfer.in.Item.Chain = append(transfer.in.Item.Chain, device.Item{
					ID:     ch.blobs[transfer.outBlobs[i]].Item.ID,
					Size:   uint64(transfer.outChunkSize[i]),
					Offset: uint64(transfer.outChunkOffset[i]),
				})
				// TODO: is it possible for a child to have a chain  ? What should happen ?
			}

		}
	}

	// Stable metadata. Now the client can safely retrieve them
	for _, transfer := range ch.transfers {
		close(transfer.in.Complete)
	}

	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, in *device.Stream) error {
	ch.transferLocker.Lock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	transfer := &transfer{
		in: in,
	}
	ch.transfers = append(ch.transfers, transfer)

	go func() {
		defer ch.transferLocker.Unlock()
		for {
			// ReadFull would return EOF at 4096. We do not want that.
			wb, err := io.ReadAtLeast(src, ch.buffer[ch.boffset:], len(ch.buffer[ch.boffset:]))
			switch {
			case err == io.EOF:
				// EOF only if no bytes were read (closed stream without data)
				return

			case err == io.ErrUnexpectedEOF:
				// If an EOF happens after reading fewer than min bytes, ReadAtLeast returns ErrUnexpectedEOF
				// Usually this happens because the stream data cannot fill a whole blob. This is normal,
				// since a blob is meant to accumulate the data of several streams
				transfer.outBlobs = append(transfer.outBlobs, len(ch.blobs))
				transfer.outChunkSize = append(transfer.outChunkSize, int64(wb))
				transfer.outChunkOffset = append(transfer.outChunkOffset, ch.boffset)

				ch.boffset += int64(wb)
				return

			case err == nil:
				if wb >= 0 {
					// The incoming data are not fully read because the blob is partially full.
					// In this case the blob must be flushed and the remaining incoming data
					// should be served by another blob
					transfer.outBlobs = append(transfer.outBlobs, len(ch.blobs))
					transfer.outChunkSize = append(transfer.outChunkSize, int64(wb))
					transfer.outChunkOffset = append(transfer.outChunkOffset, ch.boffset)

					ch.boffset += int64(wb)
				}

				if err := ch.flush(); err != nil {
					panic(err)
				}

			default:
				panic(err)
			}
		}
	}()
	return nil
}

func (ch *wchannel) flush() error {

	ch.bufLocker.Lock()
	defer ch.bufLocker.Unlock()

	if ch.boffset == 0 {
		return nil
	}

	blob := &device.Stream{Complete: make(chan struct{})}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, blob); err != nil {
		return err
	}

	_, err := io.Copy(pw, bytes.NewReader(ch.buffer[:ch.boffset]))
	if err != nil {
		return err
	}
	if err := pw.Close(); err != nil {
		return err
	}

	ch.blobs = append(ch.blobs, blob)
	ch.boffset = 0
	return nil
}
