// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package blob // import "gitlab.com/tromos/hub/device/blob/lib"

import (
	"code.cloudfoundry.org/bytefmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
)

func New(conf *viper.Viper) device.Device {

	bs, err := bytefmt.ToBytes(conf.GetString("blocksize"))
	if err != nil {
		panic(err)
	}

	return &Blob{blocksize: bs}
}

type Blob struct {
	device.Device
	blocksize uint64
}

func (blob *Blob) String() string {
	return "blob <- " + blob.Device.String()
}

func (blob *Blob) Capabilities() []selector.Capability {
	return append(blob.Device.Capabilities(), selector.BurstBuffer)
}

func (blob *Blob) SetBackend(backend device.Device) {
	if backend == nil {
		panic("NIL Device not allowed")
	}
	blob.Device = backend
}

func (blob *Blob) NewWriteChannel(name string) (device.WriteChannel, error) {
	channel, err := blob.Device.NewWriteChannel(name)
	if err != nil {
		return nil, err
	}
	return &wchannel{
		remote: channel,
		buffer: make([]byte, blob.blocksize),
	}, nil
}

func (blob *Blob) NewReadChannel(name string) (device.ReadChannel, error) {
	channel, err := blob.Device.NewReadChannel(name)
	if err != nil {
		return nil, err
	}
	return &rchannel{remote: channel}, nil
}

func (blob *Blob) Close() error {
	return blob.Device.Close()
}
