// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package blob // import "gitlab.com/tromos/hub/device/blob/lib"

import (
	"bytes"
	"gitlab.com/tromos/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel
}

func (ch *rchannel) Close() error {
	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	// Flatten the logical item into several physical items
	var chain []*device.Stream
	chain = append(chain, &device.Stream{Item: in.Item})
	chain[0].Item.Chain = nil

	for _, item := range in.Item.Chain {
		chain = append(chain, &device.Stream{Item: item})
	}

	var wg sync.WaitGroup
	bufs := make([]*bytes.Buffer, len(chain))
	for i, stream := range chain {
		pr, pw := io.Pipe()

		if err := ch.remote.NewTransfer(pw, stream); err != nil {
			return err
		}

		wg.Add(1)
		go func(i int, stream *device.Stream) {
			defer wg.Done()
			bufs[i] = bytes.NewBuffer(make([]byte, 0, stream.Item.Size))
			if _, err := io.CopyN(bufs[i], pr, int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}(i, stream)

	}

	go func() {
		wg.Wait()
		defer dst.Close()

		for i, stream := range chain {
			// Keep it like that. Otherwise EOF never comes and garbages return
			if _, err := io.CopyN(dst, bufs[i], int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}
	}()

	return nil
}
