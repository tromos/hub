// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/hub/device"

import (
	"gitlab.com/tromos/hub/selector"
)

var _ Device = (*DefaultDevice)(nil)

type DefaultDevice struct {
	closed bool
}

func (d *DefaultDevice) SetBackend(Device) {
	if d.closed {
		panic(ErrClosed)
	}
	return
}

func (d *DefaultDevice) String() string {
	if d.closed {
		panic(ErrClosed)
	}
	return "DefaultDevice"
}

func (d *DefaultDevice) Capabilities() []selector.Capability {
	if d.closed {
		panic(ErrClosed)
	}
	return nil
}

func (d *DefaultDevice) Location() string {
	if d.closed {
		panic(ErrClosed)
	}
	return "localhost"
}

func (d *DefaultDevice) NewWriteChannel(collectionName string) (WriteChannel, error) {
	if d.closed {
		panic(ErrClosed)
	}
	return nil, nil
}

func (d *DefaultDevice) NewReadChannel(string) (ReadChannel, error) {
	if d.closed {
		panic(ErrClosed)
	}

	return nil, nil
}

func (d *DefaultDevice) Scan() ([]string, []Item, error) {
	if d.closed {
		panic(ErrClosed)
	}

	return nil, nil, nil
}

func (d *DefaultDevice) Close() error {
	if d.closed {
		panic(ErrClosed)
	}
	d.closed = true

	return nil
}

