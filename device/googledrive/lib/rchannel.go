// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package googledrive // import "gitlab.com/tromos/hub/device/googledrive/lib"

import (
	"bytes"
	"github.com/prasmussen/gdrive/drive"
	"gitlab.com/tromos/hub/device"
	"io"
	"io/ioutil"
)

type rchannel struct {
	gd    *Googledrive
	chunk *bytes.Buffer
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	if ch.chunk == nil {
		ch.chunk = bytes.NewBuffer(nil)

		// Retrieve the whole chunk (potentially blob)
		req := drive.DownloadArgs{
			Progress: ioutil.Discard,
			Out:      ch.chunk,
			Id:       in.Item.ID,
			Stdout:   true, // Write data to out (instead of file)
		}
		if err := ch.gd.driver.Download(req); err != nil {
			return err
		}
	}

	go func() {
		defer dst.Close()
		_, err := io.Copy(dst, io.NewSectionReader(bytes.NewReader(ch.chunk.Bytes()), int64(in.Item.Offset), int64(in.Item.Size)))
		if err != nil {
			panic(err)
		}

	}()
	return nil
}
