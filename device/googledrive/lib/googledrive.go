// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package googledrive // import "gitlab.com/tromos/hub/device/googledrive/lib"

import (
	"context"
	"github.com/prasmussen/gdrive/drive"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/hub/pkg/oauth"
	"golang.org/x/oauth2/google"
	v3 "google.golang.org/api/drive/v3"
	"io/ioutil"
)

func New(conf *viper.Viper) device.Device {
	// Load the http client that will be used for connecting to googledrive
	b, err := ioutil.ReadFile(conf.GetString("credentials"))
	if err != nil {
		panic(err)
	}
	config, err := google.ConfigFromJSON(b, v3.DriveScope)
	if err != nil {
		panic("Unable to parse client secret file to config:" + err.Error())
	}
	client := oauth.NewClient(context.Background(), config)

	driver, err := drive.New(client)
	if err != nil {
		panic(err)
	}

	// TODO: Add clean start
	/*
		mkdirArgs := drive.MkdirArgs {
			Out: nil,
			Name:  pathDir,
			Description: "",
		}
	*/

	return &Googledrive{driver: driver}
}

type Googledrive struct {
	device.Device
	driver *drive.Drive
}

func (gd *Googledrive) SetBackend(backend device.Device) {
	gd.Device = backend
}

func (gd *Googledrive) Capabilities() []selector.Capability {
	return []selector.Capability{selector.Cloud}
}

func (gd *Googledrive) String() string {
	return "googledrive"
}

func (gd *Googledrive) NewWriteChannel(name string) (device.WriteChannel, error) {
	return &wchannel{
		gd:     gd,
		prefix: name,
	}, nil
}

func (gd *Googledrive) NewReadChannel(_ string) (device.ReadChannel, error) {
	return &rchannel{gd: gd}, nil
}

func (gd *Googledrive) Scan() ([]string, []device.Item, error) {
	return nil, nil, device.ErrNoImpl
}

func (gd *Googledrive) Close() error {
	return nil
}
