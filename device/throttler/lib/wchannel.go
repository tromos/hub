// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package throttler // import "gitlab.com/tromos/hub/device/throttler/lib"

import (
	"github.com/juju/ratelimit"
	"gitlab.com/tromos/hub/device"
	"io"
)

// Allows only for one stream
type wchannel struct {
	throttle *Throttle
	bucket   *ratelimit.Bucket
	remote   device.WriteChannel
}

func (ch *wchannel) Close() error {
	return ch.remote.Close()
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, stream *device.Stream) error {
	bucket := ch.bucket
	if ch.throttle.regulate == Stream {
		bucket = ratelimit.NewBucketWithRate(ch.throttle.rate, ch.throttle.capacity)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, stream); err != nil {
		return err
	}

	go func() {
		if _, err := io.Copy(pw, ratelimit.Reader(src, bucket)); err != nil {
			panic(err)
		}
		if err := pw.Close(); err != nil {
			panic(err)
		}
	}()

	return nil
}
