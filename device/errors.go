// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/hub/device"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrDevice  = errors.NewClass("Device Error")
	ErrArg     = ErrDevice.NewClass("Argument error")
	ErrRuntime = ErrDevice.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrDevice.New("Backend error")
	ErrNoImpl  = ErrDevice.New("Function not implemented")
	ErrProxy   = ErrDevice.New("Uncaptured PROXY ERROR")
	ErrClosed  = ErrDevice.New("Device is closed")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrChannelClosed = ErrRuntime.New("Action on closed channel")
	ErrStream        = ErrRuntime.New("Stream transfer error")
)
