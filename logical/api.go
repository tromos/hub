// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package logical // import "gitlab.com/tromos/hub/logical"

type Segment struct {
	Index uint64
	From  int64
	To    int64
}

// Logical (files) does not contain data. It contains a description of records found in one or more physical files.
type Logical interface {
	// Add adds a new update record to the tree. Offset and size indicate the affected segment of the logical file
	Add(offset int64, size int64) uint64

	// Overlap returns the indices of deltas that overlap with the request
	// When request offset is not aligned with deltas, additional bytes
	// need to be transferred. We must calculate this overhead to the buffer
	Overlaps(from int64, to int64) (size uint64, segments []Segment)

	// Length returns the contiguous space (with holes) of records
	Length() int64
}
