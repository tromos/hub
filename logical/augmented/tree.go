// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package augmented // import "gitlab.com/tromos/hub/logical/augmented"

import (
	"github.com/workiva/go-datastructures/augmentedtree"
	"gitlab.com/tromos/hub/logical"
	"sort"
)

/* Fully overlap. i completely masks iv */
func Masks(i augmentedtree.Interval, iv augmentedtree.Interval) bool {
	return i.HighAtDimension(1) >= iv.HighAtDimension(1) &&
		i.LowAtDimension(1) <= iv.LowAtDimension(1)
}

type Tree struct {
	tree     augmentedtree.Tree
	maxEntry uint64
	length   int64
}

// Not thread-safe
func NewTree(offset int64, size int64) *Tree {
	it := &Tree{
		tree:     augmentedtree.New(1),
		maxEntry: 0,
	}

	if offset != 0 || size != 0 {
		it.Add(offset, offset+size)
	}

	return it
}

// Add a new dimension and return its identifier
func (it *Tree) Add(offset int64, bytes int64) uint64 {

	index := it.maxEntry
	dim := constructSingleDimension(offset, offset+bytes-1, index)
	it.tree.Add(dim)
	it.maxEntry++

	if it.length < offset+bytes {
		it.length = offset + bytes
	}
	return index
}

// Return the segments that contribute to the contiguous space, ordered by offset
func (it *Tree) Overlaps(offset int64, bytes int64) (size uint64, segments []logical.Segment) {

	// Start from the latest-writen Segments and filter out any overlaps
	candidates := it.tree.Query(constructSingleDimension(offset, offset+bytes-1, uint64(0)))

	// Sort by ID, so to get candidates in their chronological order
	sort.SliceStable(candidates, func(i, j int) bool {
		return candidates[i].ID() < candidates[j].ID()
	})

	var lowest, highest int64 = offset + bytes, 0
	var contiguous augmentedtree.Interval
	for i := len(candidates) - 1; i >= 0; i-- {
		contiguous = constructSingleDimension(lowest, highest, uint64(0))

		// Ignore the candidate if the other (newer) candidates already fill
		// the contiguous space
		if Masks(contiguous, candidates[i]) {
			continue
		}

		if candidates[i].LowAtDimension(1) < contiguous.LowAtDimension(1) {
			lowest = candidates[i].LowAtDimension(1)
		}

		if candidates[i].HighAtDimension(1) > contiguous.HighAtDimension(1) {
			highest = candidates[i].HighAtDimension(1)
		}

		segments = append(segments, logical.Segment{
			Index: candidates[i].ID(),
			From:  candidates[i].LowAtDimension(1),
			To:    candidates[i].HighAtDimension(1),
		})
	}

	// Sort by Offset, to coverg sequentially the contiguous space
	sort.SliceStable(segments, func(i, j int) bool {
		return segments[i].From < segments[j].From
	})

	return uint64(highest - lowest), segments
}

func (it *Tree) Length() int64 {
	return it.length
}
