// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package radix // import "gitlab.com/tromos/hub/selector/radix/lib"

import (
	"github.com/hashicorp/go-immutable-radix"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/selector"
)

func New(*viper.Viper) selector.Selector {
	return &Radix{
		entries: iradix.New().Txn(),
	}
}

type Radix struct {
	entries *iradix.Txn
}

func (s *Radix) Deterministic() bool {
	return true
}

func (s *Radix) Add(p selector.Properties) {
	s.entries.Insert([]byte(p.ID), p)
}

func (s *Radix) Commit() {
	s.entries.Commit()
}

func (s *Radix) Walk(walkfn func(k []byte, v interface{}) bool) {
	s.entries.Root().Walk(walkfn)
}

func (s *Radix) Partition(key string) (string, error) {
	k, _, ok := s.entries.Root().LongestPrefix([]byte(key))
	if !ok {
		k, _, ok := s.entries.Root().Minimum()
		if !ok {
			return "", selector.ErrNoElement
		}
		return string(k), nil
	}
	return string(k), nil
}

func (s *Radix) Select(exclude []string, _ ...selector.Capability) (string, error) {
	panic("not implemented")
}
