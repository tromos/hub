// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package selector // import "gitlab.com/tromos/hub/selector"

import (
	"github.com/spf13/viper"
)

type Plugin func(conf *viper.Viper) Selector

type Properties struct {
	ID           string
	Capabilities []Capability
	Peer         string
}

type Selector interface {
	// Add includes a new entity in the pool
	Add(p Properties)

	// Commit is used when all entities have been included. Select and
	// Walk can be used unly after a selector has been committed
	Commit()

	// Partition returns the authority responsible for the element
	Partition(key string) (string, error)

	// Select one of the available entities in the pool based on the
	// given constrains.
	Select(exclude []string, criteria ...Capability) (string, error)

	// Walk is used when iterating the tree. Returns if iterations should
	// be terminated
	Walk(func(k []byte, v interface{}) bool)
}
