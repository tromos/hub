// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package consistenthash // import "gitlab.com/tromos/hub/selector/consistenthash/lib"

import (
	"github.com/golang/groupcache/consistenthash"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/selector"
)

func New(*viper.Viper) selector.Selector {
	return &ConsistentHash{
		index: consistenthash.New(1, nil),
		flat:  []selector.Properties{},
	}
}

type ConsistentHash struct {
	index *consistenthash.Map
	flat  []selector.Properties
}

func (s *ConsistentHash) Add(p selector.Properties) {
	s.flat = append(s.flat, p)
	s.index.Add(p.ID)
}

func (s *ConsistentHash) Commit() {
}

func (s *ConsistentHash) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *ConsistentHash) Partition(key string) (string, error) {
	return s.index.Get(key), nil
}

func (s *ConsistentHash) Select(exclude []string, _ ...selector.Capability) (string, error) {
	panic("not implemented")
}
