// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package selector // import "gitlab.com/tromos/hub/selector"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrSelector = errors.NewClass("Selector Error")
	ErrArg      = ErrSelector.NewClass("Argument error")
	ErrRuntime  = ErrSelector.NewClass("Runtime error")

	// Runtime family errors
	ErrExhausted = ErrRuntime.New("Elements in the pool have been exhausted")
	ErrNoElement = ErrRuntime.New("No matching element was found")
)
