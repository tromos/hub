// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package roundrobin // import "gitlab.com/tromos/hub/selector/roundrobin/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/hub/pkg/structures"
	"sync/atomic"
)

func New(*viper.Viper) selector.Selector {
	return &RoundRobin{
		flat: []selector.Properties{},
	}
}

type RoundRobin struct {
	prev int64
	flat []selector.Properties
}

func (s *RoundRobin) Add(p selector.Properties) {
	s.flat = append(s.flat, p)
}

func (s *RoundRobin) Commit() {
}

func (s *RoundRobin) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *RoundRobin) Partition(key string) (string, error) {
	panic("not implemented")
}

func (s *RoundRobin) Select(exclude []string, _ ...selector.Capability) (string, error) {

	if len(s.flat) <= len(exclude) {
		return "", selector.ErrExhausted
	}

repeat:
	next := atomic.AddInt64(&s.prev, 1)
	id := s.flat[int(next)%len(s.flat)].ID
	if structures.StringInSlice(id, exclude) {
		goto repeat
	}
	return id, nil
}
