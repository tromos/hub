// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package byqos // import "gitlab.com/tromos/hub/selector/byqos/lib"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/hub/pkg/structures"
	"sync/atomic"
)

func New(*viper.Viper) selector.Selector {
	return &ByQOS{
		indexes: make(map[selector.Capability][]selector.Properties),
	}
}

type ByQOS struct {
	seed    int64
	indexes map[selector.Capability][]selector.Properties
}

func (s *ByQOS) Add(p selector.Properties) {
	for _, capacity := range p.Capabilities {
		available := s.indexes[capacity]
		available = append(available, p)
		s.indexes[capacity] = available
	}
}

func (s *ByQOS) Commit() {}

func (s *ByQOS) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, index := range s.indexes {
		for _, v := range index {
			if walkfn([]byte(v.ID), v) {
				return
			}
		}
	}
}

func (s *ByQOS) Partition(key string) (string, error) {
	panic("not implemented")
}

func (s *ByQOS) Select(exclude []string, criteria ...selector.Capability) (string, error) {

	if len(criteria) == 0 {
		criteria = append(criteria, selector.Default)
	}

	for i := 0; i < len(criteria); i++ {
		proposed, ok := s.indexes[criteria[i]]
		if !ok {
			continue
		}

		// Start with an offset so to utilize all the elements.
		seed := atomic.AddInt64(&s.seed, 1) % int64(len(proposed))
		for j := int(seed); j < len(proposed); j++ {
			id := proposed[j].ID
			if !structures.StringInSlice(id, exclude) {
				return id, nil
			}
		}
	}
	return "", selector.ErrNoElement
}
