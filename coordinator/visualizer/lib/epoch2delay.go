// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package visualizer // import "gitlab.com/tromos/hub/coordinator/visualizer/lib"

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"sync"
	//"gonum.org/v1/plot/vg/draw"
)

type epoch2delay struct {
	locker sync.Mutex
	plot   *plot.Plot
	writes plotter.XYs
	reads  plotter.XYs
}

func (p *epoch2delay) init() {
	// Create a new figure, set its title and
	// axis labels.
	figure, err := plot.New()
	if err != nil {
		panic(err)
	}
	figure.Title.Text = "Requests"
	figure.X.Label.Text = "Request Epoch"
	figure.Y.Label.Text = "Delay (seconds)"
	// Draw a grid behind the data
	figure.Add(plotter.NewGrid())

	// Create placeholder for write points
	p.writes = plotter.XYs{}
	// Create placeholder for read points
	p.reads = plotter.XYs{}

	p.plot = figure
}

func (p *epoch2delay) addWriteRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	p.writes = append(p.writes,
		struct{ X, Y float64 }{float64(req.issue), float64(req.end - req.issue)},
	)
}

func (p *epoch2delay) addReadRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	p.reads = append(p.reads,
		struct{ X, Y float64 }{float64(req.issue), float64(req.end - req.issue)},
	)
}

func (p *epoch2delay) save(imagepath string) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Write Scatter
	ws, err := plotter.NewScatter(p.writes)
	if err != nil {
		panic(err)
	}
	ws.Color = plotutil.Color(2)
	ws.Shape = plotutil.Shape(6)

	// Read Scatter
	rs, err := plotter.NewScatter(p.reads)
	if err != nil {
		panic(err)
	}
	rs.Color = plotutil.Color(3)
	rs.Shape = plotutil.Shape(7)

	p.plot.Add(ws, rs)

	// Save the plot to a PDF file.
	err = p.plot.Save(4*vg.Inch, 4*vg.Inch, imagepath+"epoch2delay.pdf")
	if err != nil {
		panic(err)
	}
}
