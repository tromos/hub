// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package visualizer // import "gitlab.com/tromos/hub/coordinator/visualizer/lib"

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"sync"
	//"gonum.org/v1/plot/vg/draw"
)

type epoch2address struct {
	locker sync.Mutex
	plot   *plot.Plot
}

func (p *epoch2address) init() {
	// Create a new figure, set its title and
	// axis labels.
	figure, err := plot.New()
	if err != nil {
		panic(err)
	}
	figure.Title.Text = "Requests"
	figure.X.Label.Text = "Request Epoch"
	figure.Y.Label.Text = "Affected Address Range (bytes)"
	// Draw a grid behind the data
	figure.Add(plotter.NewGrid())

	p.plot = figure
}

func (p *epoch2address) addWriteRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Do not use range as it will make a copy
	for i := 0; i < len(req.UpdateRecord.Deltas); i++ {
		delta := &req.UpdateRecord.Deltas[i]

		points := plotter.XYs{
			{X: float64(req.issue), Y: float64(delta.Offset)},
			{X: float64(req.begin), Y: float64(delta.Offset)},
			{X: float64(req.end), Y: float64(delta.Offset + delta.Size)},
		}

		// Make a line plotter with points and set its style.
		wrLine, wrPoints, err := plotter.NewLinePoints(points)
		if err != nil {
			panic(err)
		}

		// Optional beautify
		useColor := plotutil.Color(2)

		wrLine.LineStyle.Width = vg.Points(1)
		wrLine.LineStyle.Dashes = []vg.Length{vg.Points(5), vg.Points(5)}
		wrLine.Color = useColor

		wrPoints.Shape = plotutil.Shape(6)
		wrPoints.Color = useColor

		// Register the line into the plot
		p.plot.Add(wrLine, wrPoints)
	}
}

func (p *epoch2address) addReadRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Do not use range as it will make a copy
	for i := 0; i < len(req.UpdateRecord.Deltas); i++ {
		delta := &req.UpdateRecord.Deltas[i]

		points := plotter.XYs{
			{X: float64(req.issue), Y: float64(delta.Offset)},
			{X: float64(req.begin), Y: float64(delta.Offset)},
			{X: float64(req.end), Y: float64(delta.Offset + delta.Size)},
		}

		// Make a line plotter with points and set its style.
		rdLine, rdPoints, err := plotter.NewLinePoints(points)
		if err != nil {
			panic(err)
		}
		// Optional beautify
		useColor := plotutil.Color(3)

		rdLine.LineStyle.Width = vg.Points(1)
		rdLine.LineStyle.Dashes = []vg.Length{vg.Points(5), vg.Points(5)}
		rdLine.Color = useColor

		rdPoints.Shape = plotutil.Shape(7)
		rdPoints.Color = useColor

		// Register the line into the plot
		p.plot.Add(rdLine, rdPoints)
	}
}

func (p *epoch2address) save(imagepath string) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Save the plot to a PDF file.
	err := p.plot.Save(4*vg.Inch, 4*vg.Inch, imagepath+"epoch2address.pdf")
	if err != nil {
		panic(err)
	}
}
